using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using agroApp_BackEnd.API.data.Repositories;
using agroApp_BackEnd.API.models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using agroapp_backend.API.Dtos;
using System.IO;
using System;
using Microsoft.AspNetCore.Http;
using agroapp_backend.API.Core.ProductManager;
using Microsoft.EntityFrameworkCore;

namespace agroApp_BackEnd.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IRepositoryWrapper _repository;
        private readonly IProductManager _productManager;
        public ProductController(IRepositoryWrapper repository, IProductManager productManager)
        {
            _repository = repository;
            _productManager = productManager;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetProductsAsync()
        {
            var products = await _productManager.GetAllAsync();

            return Ok(products);
        }

        [HttpGet("GetProductByUser/{idUser}")]
        public IActionResult GetProductByUser(int idUser)
        {
            var product = _repository.Product.FindByCondition(s => s.UserId == idUser).ToList();

            return Ok(product);
        }

        [HttpGet("GetProductAsync/{id}",Name="GetProductAsync")]
        public IActionResult GetProductAsync(int id)
        {
            var product = _repository.Product.FindByCondition(s => s.Id == id).FirstOrDefault();

            if (product != null)
            {
                return Ok(product);
            }
            else
            {
                return BadRequest();
            }
        }

        [AllowAnonymous]
        [HttpGet("GetProductsByCategory/{idCategory}")]
        public async Task<IActionResult> GetProductsByCategory(int idCategory)
        {
            var product = (await _productManager.GetAllAsync()).Where(s => s.CategoryId == idCategory).ToList();
            
            if(product != null)
            {
                return Ok(product);
            }

            return BadRequest();
        }

        [AllowAnonymous]
        [HttpGet("GetProductsByName/{name}")]
        public async Task<IActionResult> GetProductsByName(string name)
        {
            var product = (await _productManager.GetAllAsync()).Where(s => s.Name.ToLower().Contains(name.ToLower())).ToList();

            if (product != null)
            {
                return Ok(product);
            }

            return BadRequest();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> CreateProductAsync(AddProduct product)
        {
            var prod = new Product
            {
                Name = product.Name,
                Description = product.Description,
                CategoryId = product.CategoryId,
                UserId = product.UserId,
                Price = product.Price
            };



            await _repository.Product.CreateAsync(prod);
            await _repository.SaveChangesAsync();
            return new CreatedAtRouteResult("GetProductAsync", new { id = prod.Id }, prod);
        }

        [HttpPut("photo/{id}")]
        public async Task<IActionResult> UpdatePhoto(int id, [FromForm] IFormFile file)
        {
            var product = _repository.Product.FindByCondition(s => s.Id == id).FirstOrDefault();
            if (product != null)
            {
                var f = file;

                if (f != null && f.Length > 0)
                {
                    byte[] p1 = null;
                    using (var fs1 = f.OpenReadStream())
                    using (var ms1 = new MemoryStream())
                    {
                        fs1.CopyTo(ms1);

                        p1 = ms1.ToArray();
                    }
                    product.Image = $"data:image/jpeg;base64,{Convert.ToBase64String(p1)}";
                }

                _repository.Product.Update(product);
                await _repository.SaveChangesAsync();
                return Ok(product.Image);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
