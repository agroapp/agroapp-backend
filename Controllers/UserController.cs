using System.Threading.Tasks;
using agroapp_backend.API.Core.UserManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace agroapp_backend.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserManager _userManager;
        public UserController(IUserManager userManager)
        {
            _userManager = userManager;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var user = _userManager.GetUserInfo(id);

            if (user == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(user);
            }
        }

        [HttpPut("updatePicture/{id}")]
        public async Task<IActionResult> UpdatePicture(int id, [FromForm] IFormFile photo)
        {
            if (photo == null || photo.Length == 0)
            {
                return BadRequest("foto vacia");
            }

            var user = await _userManager.UpdateUserPictureAsync(id, photo);

            return Ok(user);
        }
    }
}