using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using agroapp_backend.API.Core.ProfileManager;
using agroApp_BackEnd.API.data;
using agroApp_BackEnd.API.models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace agroApp_BackEnd.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly IProfileManager _profileManager;
        public ProfileController(IProfileManager profileManager)
        {
            _profileManager = profileManager;
        }

        [HttpGet]
        public async Task<IActionResult> GetProfileAsync()
        {
            var prof = await _profileManager.GetAllAsync();

            return Ok(prof);
        }

        [HttpGet("{idProfile}")]
        //[HttpGet("{id}", Name = "GetProductAsync")]
        public IActionResult GetProfileAsync(int idProfile)
        {
            var prof = _profileManager.GetById(idProfile);
            return Ok(prof);
        }

        [HttpPut]
        public IActionResult UpdateProfileAsync(Profile profile)
        {
            var result = _profileManager.UpdateProfile(profile);

            if (result)
            {
                return Ok();
            }
            return NotFound();
        }

        [HttpDelete("{idProfile}")]
        public IActionResult DeleteProfile(int idProfile)
        {
            var profile = _profileManager.GetById(idProfile);
            if (profile != null)
            {
                var result = _profileManager.DeleteProfile(profile);

                if (result)
                {
                    return Ok();
                }

                return NotFound();
            }

            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> CreateProfileAsync(Profile profile)
        {
            var prof = await _profileManager.CreateProfileAsync(profile);

            return CreatedAtAction("CreateProfileAsync", prof);
        }
    }
}