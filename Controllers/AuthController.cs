using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using agroApp_BackEnd.API.Core.AuthManager;
using agroApp_BackEnd.API.data.Repositories;
using agroApp_BackEnd.API.Dtos;
using agroApp_BackEnd.API.models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace agroApp_BackEnd.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthRepository _authRepository;
        private readonly IAuthManager _authManager;
        
        public AuthController(IAuthRepository authRepository,IAuthManager authManager)
        {
            _authRepository = authRepository;
            _authManager = authManager;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]UserForRegister user)
        {
            // Validar el usuario
            user.UserName = user.UserName.ToLower();

            if (await _authRepository.UserExist(user.UserName))
            {
                return BadRequest("El nombre de usuario ya existe.");
            }

            var userToCreate = new User
            {
                UserName = user.UserName
            };

            var createdUser = await _authRepository.Register(userToCreate, user.Password);
            return StatusCode(201);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserForLogin user)
        {
            var userFromRepo = await _authRepository.Login(user.Username.ToLower(), user.Password);

            if (userFromRepo == null)
            {
                return Unauthorized("Usuario o contraseña incorrectos.");
            }

            return Ok(new {
                token = _authManager.login(userFromRepo)
            });
        }

        [HttpGet("validateUser/{id}")]
        public async Task<IActionResult> ValidateUser(string id){
            var user = await _authRepository.FindByUserName(id);

            if(user != null)
                return Ok(user);

            return BadRequest();
        }
    }
}