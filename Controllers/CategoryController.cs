using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using agroapp_backend.API.Core.CategoryManager;
using agroApp_BackEnd.API.data;
using agroApp_BackEnd.API.models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace agroApp_BackEnd.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryManager _categoryManager;
        public CategoryController(ICategoryManager categoryManager)
        {
            _categoryManager = categoryManager;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetCategoryAsync()
        {
            var cat = await _categoryManager.GetAllAsync();

            return Ok(cat);
        }

        [HttpGet("{idCategory}")]
        //[HttpGet("{id}", Name = "GetProductAsync")]
        public IActionResult GetCategoryAsync(int idCategory)
        {
            var cat = _categoryManager.GetById(idCategory);
            return Ok(cat);
        }

        [HttpPut]
        public IActionResult UpdateCategoryAsync(Category category)
        {
            var result = _categoryManager.UpdateCategory(category);

            if (result)
            {
                return Ok();
            }
            return NotFound();
        }

        [HttpDelete("{idCategory}")]
        public IActionResult DeleteCategory(int idCategory)
        {
            var category = _categoryManager.GetById(idCategory);
            if (category != null)
            {
                var result = _categoryManager.DeleteCategory(category);

                if (result)
                {
                    return Ok();
                }

                return NotFound();
            }

            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> CreateCategoryAsync(Category category)
        {
            var cat = await _categoryManager.CreateCategoryAsync(category);

            return CreatedAtAction("CreateCategoryAsync", cat);
        }
    }
}