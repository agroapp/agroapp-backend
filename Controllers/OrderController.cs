using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using agroapp_backend.API.Core.OrderManager;
using agroApp_BackEnd.API.data;
using agroApp_BackEnd.API.models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace agroApp_BackEnd.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderManager _orderManager;
        public OrderController(IOrderManager orderManager)
        {
            _orderManager = orderManager;
        }

        [HttpGet]
        public async Task<IActionResult> GetOrderAsync()
        {
            var ord = await _orderManager.GetAllAsync();

            return Ok(ord);
        }

        [HttpGet("{idOrder}")]
        //[HttpGet("{id}", Name = "GetProductAsync")]
        public IActionResult GetOrderAsync(int idOrder)
        {
            var ord = _orderManager.GetById(idOrder);
            return Ok(ord);
        }

        [HttpPut]
        public IActionResult UpdateOrderAsync(Order order)
        {
            var result = _orderManager.UpdateOrder(order);

            if (result)
            {
                return Ok();
            }
            return NotFound();
        }

        [HttpDelete("{idOrder}")]
        public IActionResult DeleteOrder(int idOrder)
        {
            var order = _orderManager.GetById(idOrder);
            if (order != null)
            {
                var result = _orderManager.DeleteOrder(order);

                if (result)
                {
                    return Ok();
                }

                return NotFound();
            }

            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> CreateOrderAsync(Order order)
        {
            var ord = await _orderManager.CreateOrderAsync(order);

            return CreatedAtAction("CreateOrderAsync", ord);
        }
    }
}