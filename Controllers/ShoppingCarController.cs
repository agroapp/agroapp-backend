﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using agroapp_backend.API.Core.ProductManager;
using agroapp_backend.API.models;
using agroApp_BackEnd.API.Core.ShoppingCardManager;
using agroApp_BackEnd.API.models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace agroApp_BackEnd.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ShoppingCarController : ControllerBase
    {
        private readonly IShoppingCarManager _shoppingCarManager;
        private readonly IProductManager _productManager;
        public ShoppingCarController(IShoppingCarManager shopping, IProductManager product)
        {
            _shoppingCarManager = shopping;
            _productManager = product;
        }

        [HttpGet("{idUsuario}")]
        public IActionResult GetShoppingCarAsync(int idUsuario)
        {
            var model = _shoppingCarManager.GetShoppingCar(idUsuario);

            var products = new List<Product>();

            float productoTotal = 0;

            foreach(var pro in model.ShopingCarDetails)
            {
                var product = _productManager.GetById(pro.ProductId);

                productoTotal = productoTotal + (product.Price * pro.Quantity);

                products.Add(product);
            }


            float envio = productoTotal == 0 ? 0 : 500;
            float subtotal = envio + productoTotal;
            float isv = (float)(subtotal * 0.15);
            float total = subtotal + isv;


            var productDetail = new ShoppingCarResult
            {
                envio = envio,
                productos = productoTotal,
                subtotal = subtotal,
                isv = isv,
                total = total
            };



            return Ok(new { shopping = model, product = products, factura = productDetail });
        }

        [HttpPut]
        public IActionResult UpdateShoppingCardDetail(ShoppingCarDetail shoppingCarDetail)
        {
            var result = _shoppingCarManager.UpdateShoppingCardDetail(shoppingCarDetail);

            if (result)
            {
                return Ok();
            }

            return NotFound();
        }

        [HttpDelete("{idShoppingCarDetail}")]
        public IActionResult DeleteShoppingCarDetail(int idShoppingCarDetail)
        {
            var shoppingDetail = _shoppingCarManager.GetShoppingCarDetail(idShoppingCarDetail);
            if(shoppingDetail != null)
            {
                var result = _shoppingCarManager.DeleteShoppingCardDetail(shoppingDetail);

                if (result)
                {
                    return Ok();
                }

                return NotFound();
            }

            return NotFound();


        }

        [HttpPost]
        public async Task<IActionResult> CreateShoppingCar(ShoppingCar shopping)
        {
            var cat = await _shoppingCarManager.CrearShoppingCar(shopping);

            return CreatedAtAction("CreateShoppingCar", cat);
        }

        [HttpPost("addShoppingCar")]
        public async Task<IActionResult> AddShoppingCarDetail(addProductShoppingCar product)
        {
            try
            {
                var shoppingCar = _shoppingCarManager.GetShoppingCar(product.userId).Id;

                var existeProduct = _shoppingCarManager.ExisteProductoShoppingCar(shoppingCar, product.ProductId);

                if(existeProduct == null)
                {
                    var detail = new ShoppingCarDetail
                    {
                        ProductId = product.ProductId,
                        ShoppingCarId = shoppingCar,
                        Quantity = product.Quantity
                    };

                    Console.WriteLine(detail);

                    var cat = await _shoppingCarManager.AddShoppingCarDetail(detail);

                    return CreatedAtAction("AddShoppingCarDetail", cat);
                }
                else
                {
                    var nuevaCantidad = existeProduct.Quantity + product.Quantity;
                    existeProduct.Quantity = nuevaCantidad;


                    var cat = _shoppingCarManager.UpdateShoppingCardDetail(existeProduct);

                    return CreatedAtAction("AddShoppingCarDetail", existeProduct);


                }

            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
