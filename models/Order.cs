using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace agroApp_BackEnd.API.models
{
    public class Order
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }

        public virtual Profile UserVendorId { get; set; }

        public virtual Profile UserBuyerId { get; set; }

        public Product[] Products { get; set; }

        public virtual User ProfilePicture { get; set; }

        public string Description { get; set; }

        public int CategoryId { get; set; }
    }
}