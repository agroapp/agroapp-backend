﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace agroApp_BackEnd.API.models
{
    public class ShoppingCarResult
    {
        public float productos { get; set; }
        public float envio { get; set; }
        public float subtotal { get; set; }
        public float isv { get; set; }
        public float total { get; set; }
    }
}
