using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using agroApp_BackEnd.API.models;

namespace agroapp_backend.API.models
{
    public class ShoppingCar
    {
        public int Id { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<ShoppingCarDetail> ShopingCarDetails { get; set; }
    }
}