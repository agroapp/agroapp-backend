using System.Collections.Generic;

namespace agroApp_BackEnd.API.models
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string ProfilePicture { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}