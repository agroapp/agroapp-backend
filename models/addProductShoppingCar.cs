﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace agroApp_BackEnd.API.models
{
    public class addProductShoppingCar
    {
        public int ProductId { get; set; }
        public int userId { get; set; }
        public int Quantity { get; set; }
    }
}
