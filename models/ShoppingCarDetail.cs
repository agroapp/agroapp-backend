using System.ComponentModel.DataAnnotations.Schema;
using agroApp_BackEnd.API.models;

namespace agroapp_backend.API.models
{
    public class ShoppingCarDetail
    {
        public int Id { get; set; }

        [ForeignKey("Product")]
        public int ProductId { get; set; }

        [ForeignKey("ShoppingCar")]
        public int ShoppingCarId { get; set; }

        public int Quantity { get; set; }

        public virtual Product Product { get; set; }

        public virtual ShoppingCar ShoppingCar { get; set; }
    }
}