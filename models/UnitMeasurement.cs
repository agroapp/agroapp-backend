using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace agroApp_BackEnd.API.models
{
    public class UnitMeasurement
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Product> Products { get; set; }  
    }
}