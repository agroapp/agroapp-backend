using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace agroApp_BackEnd.API.models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public float Price { get; set; }

        public string Image { get; set; }

        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }

        [ForeignKey("UnitMeasurement")]
        public int UnitMeasurementId { get; set; }

        public virtual Category Category { get; set; }

        public virtual User User { get; set; }

        public virtual UnitMeasurement UnitMeasurement { get; set; }
    }
}