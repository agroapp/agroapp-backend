using agroapp_backend.API.Core.CategoryManager;
using agroapp_backend.API.Core.UserManager;
using agroApp_BackEnd.API.Core.AuthManager;
using agroApp_BackEnd.API.data.Repositories;
using agroapp_backend.API.Core.OrderManager;
using agroapp_backend.API.Core.ProfileManager;
using agroapp_backend.API.Core.ProductManager;
using agroapp_backend.API.Core.UnitMeasurementManager;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace agroApp_BackEnd.API
{
    public static class Injection
    {
        public static void Inject(IServiceCollection services, IConfiguration Configuration)
        {
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
            services.AddScoped<IAuthRepository, AuthRepository>();
            services.AddScoped<ICategoryManager, CategoryManager>();
            services.AddScoped<IAuthManager, AuthManager>();
            services.AddScoped<IUserManager, UserManager>();
            services.AddScoped<IOrderManager, OrderManager>();
            services.AddScoped<IProfileManager, ProfileManager>();
            services.AddScoped<IProductManager, ProductManager>();
            services.AddScoped<IUnitMeasurementManager, UnitMeasurementManager>();        }
    }
}