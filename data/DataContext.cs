using agroapp_backend.API.models;
using agroApp_BackEnd.API.models;
using Microsoft.EntityFrameworkCore;

namespace agroApp_BackEnd.API.data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<UnitMeasurement> UnitMeasurements { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<ShoppingCarDetail> ShoppingCarDetails { get; set; }
        public DbSet<ShoppingCar> ShoppingCars { get; set; }
    }
}