using System.Threading.Tasks;
using agroApp_BackEnd.API.models;

namespace agroApp_BackEnd.API.data.Repositories
{
    public interface IAuthRepository
    {
         Task<User> FindByUserName(string userName);
         Task<User> Register(User user, string password);
         Task<User> Login(string userName, string password);
         Task<bool> UserExist(string userName);
    }
}