﻿using agroapp_backend.API.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace agroApp_BackEnd.API.data.Repositories
{
    public class ShoppingCarRepository : RepositoryBase<ShoppingCar>
    {
        public ShoppingCarRepository(DataContext context)
            :base(context)
        { }
    }
}
