using agroApp_BackEnd.API.models;

namespace agroApp_BackEnd.API.data.Repositories
{
    public class UnitMeasurementRepository : RepositoryBase<UnitMeasurement>
    {
        public UnitMeasurementRepository(DataContext context)
        : base(context)
        {
        }
    }
}