﻿using agroapp_backend.API.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace agroApp_BackEnd.API.data.Repositories
{
    public class ShoppingCardDetailRepository : RepositoryBase<ShoppingCarDetail>
    {
        public ShoppingCardDetailRepository(DataContext context)
            : base(context) { }
    }
}
