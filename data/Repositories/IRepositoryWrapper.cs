using System.Threading.Tasks;

namespace agroApp_BackEnd.API.data.Repositories
{
    public interface IRepositoryWrapper
    {
        UserRepository User { get; }
        ProductRepository Product { get; }
        CategoryRepository Category { get; }
        OrderRepository Order { get; }
        ProfileRepository Profile { get; }
        ShoppingCarRepository shoppingCar { get; }
        ShoppingCardDetailRepository shoppingCardDetail { get; }
        UnitMeasurementRepository unitMeasurement { get; }

        Task SaveChangesAsync();
    }
}