using System.Threading.Tasks;

namespace agroApp_BackEnd.API.data.Repositories
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private DataContext _context;
        private ProductRepository _product;
        private UserRepository _user;
        private CategoryRepository _category;
        private OrderRepository _order;
        private ProfileRepository _profile;
        private ShoppingCarRepository _shoppingCar;
        private ShoppingCardDetailRepository _shoppingCardDetail;
        private UnitMeasurementRepository _unitMeasurement;


        public RepositoryWrapper(DataContext context)
        {
            _context = context;
        }

        public UserRepository User {
            get {
                if(_user == null){
                    _user = new UserRepository(_context);
                }
                return _user;
            }
        }

        public ProductRepository Product {
            get {
                if(_product == null){
                    _product = new ProductRepository(_context);
                }
                return _product;
            }
        }

        public CategoryRepository Category {
            get {
                if(_category == null){
                    _category = new CategoryRepository(_context);
                }
                return _category;
            }
        }

        public OrderRepository Order {
            get {
                if(_order == null){
                    _order = new OrderRepository(_context);
                }
                return _order;
            }
        }

        public ProfileRepository Profile {
            get {
                if(_profile == null){
                    _profile = new ProfileRepository(_context);
                }
                return _profile;
            }
        }

        public ShoppingCarRepository shoppingCar
        {
            get
            {
                if (_shoppingCar == null)
                {
                    _shoppingCar = new ShoppingCarRepository(_context);
                }
                return _shoppingCar;
            }
        }

        public ShoppingCardDetailRepository shoppingCardDetail 
        {
            get
            {
                if (_shoppingCardDetail == null)
                {
                    _shoppingCardDetail = new ShoppingCardDetailRepository(_context);
                }
                return _shoppingCardDetail;
            }
        }

        public UnitMeasurementRepository unitMeasurement {
            get {
                if(_unitMeasurement == null){
                    _unitMeasurement = new UnitMeasurementRepository(_context);
                }
                return _unitMeasurement;
            }
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}