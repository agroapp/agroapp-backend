using agroApp_BackEnd.API.models;

namespace agroApp_BackEnd.API.data.Repositories
{
    public class ProfileRepository : RepositoryBase<Profile>
    {
        public ProfileRepository(DataContext context) 
            : base(context)
        {}
    }
}