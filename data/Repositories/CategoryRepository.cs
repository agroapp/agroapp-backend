using agroApp_BackEnd.API.models;

namespace agroApp_BackEnd.API.data.Repositories
{
    public class CategoryRepository : RepositoryBase<Category>
    {
        public CategoryRepository(DataContext context)
        : base(context)
        {
        }
    }
}