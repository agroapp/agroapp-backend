using agroApp_BackEnd.API.models;

namespace agroApp_BackEnd.API.data.Repositories
{
    public class UserRepository : RepositoryBase<User>
    {
        public UserRepository(DataContext context) : base(context)
        {
        }
    }
}