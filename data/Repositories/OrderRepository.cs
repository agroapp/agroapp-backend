using agroApp_BackEnd.API.models;

namespace agroApp_BackEnd.API.data.Repositories
{
    public class OrderRepository : RepositoryBase<Order>
    {
        public OrderRepository(DataContext context) 
            : base(context)
        {}
    }
}