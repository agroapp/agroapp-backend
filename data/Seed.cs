using System.Linq;
using System.Threading.Tasks;
using agroApp_BackEnd.API.data;
using agroApp_BackEnd.API.data.Repositories;
using agroApp_BackEnd.API.models;

namespace agroapp_backend.API.data
{
    public class Seed
    {
        public static async void SeedDatabase(DataContext context, IRepositoryWrapper repository, IAuthRepository authRepository)
        {
            await CreateUser(context, repository, authRepository);
            await CreateCategory(context, repository);
            await CreateUnitMeasureAsync(context, repository);
            await CreateProductsAsync(context, repository);
        }

        private static async Task CreateUser(DataContext context, IRepositoryWrapper repository, IAuthRepository authRepository)
        {
            if (!context.Users.Any())
            {
                var user = new User
                {
                    UserName = "admin",
                };

                await authRepository.Register(user, "Qwerty1234@");
                await repository.SaveChangesAsync();
            }
        }

        private static async Task CreateCategory(DataContext context, IRepositoryWrapper repository)
        {
            if (!context.Categories.Any())
            {
                await repository.Category.CreateAsync(new Category { Id = 1, Name = "Frutas"});
                await repository.Category.CreateAsync(new Category { Id = 2, Name = "Verduras"});
                await repository.Category.CreateAsync(new Category { Id = 3, Name = "Lácteos"});
                await repository.Category.CreateAsync(new Category { Id = 4, Name = "Tubérculos"});
                await repository.Category.CreateAsync(new Category { Id = 5, Name = "Frutos secos"});
                await repository.Category.CreateAsync(new Category { Id = 6, Name = "Frutos rojos"});
                await repository.Category.CreateAsync(new Category { Id = 7, Name = "Cítricos"});
                await repository.Category.CreateAsync(new Category { Id = 8, Name = "Granos"});

                await repository.SaveChangesAsync();
            }
        }

        private static async Task CreateUnitMeasureAsync(DataContext context, IRepositoryWrapper repository)
        {
            if (!context.UnitMeasurements.Any()) {
                await repository.unitMeasurement.CreateAsync(new UnitMeasurement { Id = 1, Name = "Libra/s" });
                await repository.unitMeasurement.CreateAsync(new UnitMeasurement { Id = 2, Name = "Kilogramo/s" });
                await repository.unitMeasurement.CreateAsync(new UnitMeasurement { Id = 3, Name = "Unidad/es" });
                await repository.unitMeasurement.CreateAsync(new UnitMeasurement { Id = 4, Name = "Caja/s" });
                await repository.SaveChangesAsync();
            }
        }

        private static async Task CreateProductsAsync(DataContext context, IRepositoryWrapper repository) {
            if(!context.Products.Any())
            {
                await repository.Product.CreateAsync(new Product {
                    Id = 1,
                    Name = "Fresa",
                    UserId = 1,
                    Price = 10.00f,
                    CategoryId = 1,
                    UnitMeasurementId = 1,
                    Description = "La mera fresa",
                    Image = "https://i.pinimg.com/originals/30/90/f6/3090f63b47d642ac1be6b059e2820946.png"
                });
                await repository.Product.CreateAsync(new Product
                {
                    Id = 2,
                    Name = "Mango",
                    UserId = 1,
                    Price = 5.00f,
                    CategoryId = 2,
                    UnitMeasurementId = 3,
                    Description = "El mero Mango",
                    Image = "https://www.pngonly.com/wp-content/uploads/2017/05/Mango-Tropical-PNG.png"
                });
                await repository.Product.CreateAsync(new Product
                {
                    Id = 3,
                    Name = "Manzana",
                    UserId = 1,
                    Price = 205.00f,
                    CategoryId = 3,
                    UnitMeasurementId = 3,
                    Description = "La mera manzana",
                    Image = "https://clipartart.com/images/apple-clipart-with-transparent-background-13.png"
                });
                await repository.Product.CreateAsync(new Product
                {
                    Id = 4,
                    Name = "Mango",
                    UserId = 1,
                    Price = 5.00f,
                    CategoryId = 4,
                    UnitMeasurementId = 3,
                    Description = "El mero Mango",
                    Image = "https://purepng.com/public/uploads/large/purepng.com-mangomangofruithalvedfrontalfullsweetfresh-481521564634qp959.png"
                });
                await repository.Product.CreateAsync(new Product
                {
                    Id = 5,
                    Name = "Mango",
                    UserId = 1,
                    Price = 5.00f,
                    CategoryId = 5,
                    UnitMeasurementId = 3,
                    Description = "El mero Mango",
                    Image = "https://www.pngonly.com/wp-content/uploads/2017/05/Mango-Tropical-PNG.png"
                });
                await repository.Product.CreateAsync(new Product
                {
                    Id = 6,
                    Name = "Mango",
                    UserId = 1,
                    Price = 5.00f,
                    CategoryId = 6,
                    UnitMeasurementId = 3,
                    Description = "El mero Mango",
                    Image = "https://purepng.com/public/uploads/large/purepng.com-mangomangofruithalvedfrontalfullsweetfresh-481521564634qp959.png"
                });
                await repository.SaveChangesAsync();
            }
        }
    }
}