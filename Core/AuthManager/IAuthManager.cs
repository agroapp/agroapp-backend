using System.IdentityModel.Tokens.Jwt;
using agroApp_BackEnd.API.models;

namespace agroApp_BackEnd.API.Core.AuthManager
{
    public interface IAuthManager
    {
        string login(User user);
    }
}