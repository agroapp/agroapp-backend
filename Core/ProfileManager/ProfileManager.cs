using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using agroApp_BackEnd.API.data.Repositories;
using agroApp_BackEnd.API.models;

namespace agroapp_backend.API.Core.ProfileManager
{
    public class ProfileManager : IProfileManager
    {
        private readonly IRepositoryWrapper _repositoryWrapper;
        public ProfileManager(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }

        public async Task<IEnumerable<Profile>> GetAllAsync()
        {
            return await _repositoryWrapper.Profile.GetAllAsync(); 
        }

        public Profile GetById(int idProfile) //*******IMPORTANTE******** Se usa el UserID
        {
            return _repositoryWrapper.Profile.FindByCondition(x => x.UserId == idProfile).FirstOrDefault();
        }

        public async Task<Profile> CreateProfileAsync(Profile profile)
        {
            await _repositoryWrapper.Profile.CreateAsync(profile);
            await _repositoryWrapper.SaveChangesAsync();
            return profile;
        }

        public bool UpdateProfile(Profile profile)
        {
            try
            {
                _repositoryWrapper.Profile.Update(profile);
                return true;
            }
           catch (Exception ex)
            {
                Console.WriteLine(ex);
            } 
            { return false; }
           
        }

        public bool DeleteProfile(Profile profile)
        {
            try
            {
                _repositoryWrapper.Profile.Delete(profile);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            } 
            { return false; }
        }

    }
}