using System.Collections.Generic;
using System.Threading.Tasks;
using agroApp_BackEnd.API.models;

namespace agroapp_backend.API.Core.ProfileManager
{
    public interface IProfileManager
    {
         Task<Profile> CreateProfileAsync(Profile profile);

        Task<IEnumerable<Profile>> GetAllAsync();

        Profile GetById(int idProfile);

        bool UpdateProfile(Profile profile);

        bool DeleteProfile(Profile profile);

    }
}