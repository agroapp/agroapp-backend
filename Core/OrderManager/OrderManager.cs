using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using agroApp_BackEnd.API.data.Repositories;
using agroApp_BackEnd.API.models;

namespace agroapp_backend.API.Core.OrderManager
{
    public class OrderManager : IOrderManager
    {
        private readonly IRepositoryWrapper _repositoryWrapper;
        public OrderManager(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }

        public async Task<IEnumerable<Order>> GetAllAsync()
        {
            return await _repositoryWrapper.Order.GetAllAsync(); 
        }

        public Order GetById(int idOrder)
        {
            return _repositoryWrapper.Order.FindByCondition(x => x.Id == idOrder).FirstOrDefault();
        }

        public async Task<Order> CreateOrderAsync(Order order)
        {
            await _repositoryWrapper.Order.CreateAsync(order);
            await _repositoryWrapper.SaveChangesAsync();
            return order;
        }

        public bool UpdateOrder(Order order)
        {
            try
            {
                _repositoryWrapper.Order.Update(order);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            } 
            { return false; }
           
        }

        public bool DeleteOrder(Order order)
        {
            try
            {
                _repositoryWrapper.Order.Delete(order);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            } 
            { return false; }
        }
    }
}