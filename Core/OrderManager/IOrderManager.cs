using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using agroApp_BackEnd.API.data.Repositories;
using agroApp_BackEnd.API.models;

namespace agroapp_backend.API.Core.OrderManager
{
    public interface IOrderManager
    {
       Task<Order> CreateOrderAsync(Order order);

        Task<IEnumerable<Order>> GetAllAsync();

        Order GetById(int idOrder);

        bool UpdateOrder(Order order);

        bool DeleteOrder(Order order);
    }
}