using System.Collections.Generic;
using System.Threading.Tasks;
using agroApp_BackEnd.API.models;

namespace agroapp_backend.API.Core.ProductManager
{
    public interface IProductManager
    {
         Task<Product> CreateProductAsync(Product product);

        Task<IEnumerable<Product>> GetAllAsync();

        Product GetById(int idProduct);

        bool UpdateProduct(Product product);

        bool DeleteProduct(Product product);

    }
}