using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using agroApp_BackEnd.API.data;
using agroApp_BackEnd.API.data.Repositories;
using agroApp_BackEnd.API.models;
using Microsoft.EntityFrameworkCore;

namespace agroapp_backend.API.Core.ProductManager
{
    public class ProductManager : IProductManager
    {
        private readonly IRepositoryWrapper _repositoryWrapper;
        private readonly DataContext _context;
        public ProductManager(IRepositoryWrapper repositoryWrapper, DataContext dataContext)
        {
            _repositoryWrapper = repositoryWrapper;
            _context = dataContext;
        }

        public async Task<IEnumerable<Product>> GetAllAsync()
        {
            return await _context.Products
                .Include(s => s.Category)
                .Include(s => s.UnitMeasurement).ToListAsync();
        }

        public Product GetById(int idProduct)
        {
            return _repositoryWrapper.Product.FindByCondition(x => x.Id == idProduct).FirstOrDefault();
        }

        public async Task<Product> CreateProductAsync(Product product)
        {
            await _repositoryWrapper.Product.CreateAsync(product);
            await _repositoryWrapper.SaveChangesAsync();
            return product;
        }

        public bool UpdateProduct(Product product)
        {
            try
            {
                _repositoryWrapper.Product.Update(product);
                return true;
            } 
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            } 
            { return false; }
           
        }

        public bool DeleteProduct(Product product)
        {
            try
            {
                _repositoryWrapper.Product.Delete(product);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            } 
            { return false; }
        }

    }
}