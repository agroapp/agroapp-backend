﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using agroapp_backend.API.models;
using agroApp_BackEnd.API.data.Repositories;

namespace agroApp_BackEnd.API.Core.ShoppingCardManager
{
    public class ShoppingCarManager : IShoppingCarManager
    {
        private readonly IRepositoryWrapper _repositoryWrapper;

        public ShoppingCarManager(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }


        public async Task<ShoppingCarDetail> AddShoppingCarDetail(ShoppingCarDetail shoppingCarDetail)
        {
            await _repositoryWrapper.shoppingCardDetail.CreateAsync(shoppingCarDetail);
            await _repositoryWrapper.SaveChangesAsync();
            return shoppingCarDetail;
        }

        public async Task<ShoppingCar> CrearShoppingCar(ShoppingCar shoppingCar)
        {
            await _repositoryWrapper.shoppingCar.CreateAsync(shoppingCar);
            await _repositoryWrapper.SaveChangesAsync();
            return shoppingCar;
        }

        public bool DeleteShoppingCardDetail(ShoppingCarDetail shoppingCarDetail)
        {
            try
            {
                _repositoryWrapper.shoppingCardDetail.Delete(shoppingCarDetail);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            { return false; }
        }

        public ShoppingCar GetShoppingCar(int idUsuario)
        {
            var model = _repositoryWrapper.shoppingCar.FindByCondition(x => x.Id == idUsuario)
                  .FirstOrDefault();

            model.ShopingCarDetails = _repositoryWrapper.shoppingCardDetail.FindByCondition(x => x.ShoppingCarId == model.Id).ToList();

            return model;

        }

        public bool UpdateShoppingCardDetail(ShoppingCarDetail shoppingCarDetail)
        {
            try
            {
                _repositoryWrapper.shoppingCardDetail.Update(shoppingCarDetail);

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            { return false; }
        }

        public ShoppingCarDetail GetShoppingCarDetail(int idShoppingDetail)
        {
            var model = _repositoryWrapper.shoppingCardDetail.FindByCondition(x => x.Id == idShoppingDetail).FirstOrDefault();

            return model;
        }

        public ShoppingCarDetail ExisteProductoShoppingCar(int idShopping, int idProudct)
        {
            var model = _repositoryWrapper.shoppingCardDetail.FindByCondition(x => x.ShoppingCarId == idShopping && x.ProductId == idProudct).FirstOrDefault();
            _repositoryWrapper.SaveChangesAsync();
            return model;

        }

    }
}
