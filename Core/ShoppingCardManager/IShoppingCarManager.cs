﻿using agroapp_backend.API.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace agroApp_BackEnd.API.Core.ShoppingCardManager
{
    public interface IShoppingCarManager
    {
        Task<ShoppingCar> CrearShoppingCar(ShoppingCar shoppingCar);

        ShoppingCar GetShoppingCar(int idUsuario);

        Task<ShoppingCarDetail> AddShoppingCarDetail(ShoppingCarDetail shoppingCarDetail);

        bool UpdateShoppingCardDetail(ShoppingCarDetail shoppingCarDetail);

        bool DeleteShoppingCardDetail(ShoppingCarDetail shoppingCarDetail);

        ShoppingCarDetail GetShoppingCarDetail(int idShoppingDetail);


        ShoppingCarDetail ExisteProductoShoppingCar(int idShopping, int idProudct);


    }
}
