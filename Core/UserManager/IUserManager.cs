using System.Threading.Tasks;
using agroapp_backend.API.Dtos;
using Microsoft.AspNetCore.Http;

namespace agroapp_backend.API.Core.UserManager
{
    public interface IUserManager
    {
        UserInfo GetUserInfo(int id);

        Task<UserInfo> UpdateUserPictureAsync(int id, IFormFile picture);
    }
}