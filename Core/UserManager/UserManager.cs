using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using agroapp_backend.API.Dtos;
using agroApp_BackEnd.API.data.Repositories;
using agroApp_BackEnd.API.models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace agroapp_backend.API.Core.UserManager
{
    public class UserManager : IUserManager
    {
        private readonly IRepositoryWrapper _repository;

        public UserManager(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        public UserInfo GetUserInfo(int id)
        {
            var user = _repository.User.FindByCondition(s => s.Id == id).FirstOrDefault();

            if (user == null)
            {
                return null;
            }

            return new UserInfo
            {
                Id = user.Id,
                UserName = user.UserName,
                Products = user.Products,
                ProfilePicture = user.ProfilePicture
            };
        }

        public async Task<UserInfo> UpdateUserPictureAsync(int id, IFormFile picture)
        {
            var user = _repository.User.FindByCondition(s => s.Id == id).FirstOrDefault();

            if (user == null)
            {
                return null;
            }

            var f = picture;

            if (f != null && f.Length > 0)
            {
                byte[] p1 = null;
                using (var fs1 = f.OpenReadStream())
                using (var ms1 = new MemoryStream())
                {
                    fs1.CopyTo(ms1);

                    p1 = ms1.ToArray();
                }
                user.ProfilePicture = $"data:image/jpeg;base64,{Convert.ToBase64String(p1)}";
            }

            _repository.User.Update(user);
            await _repository.SaveChangesAsync();

            return new UserInfo
            {
                Id = user.Id,
                UserName = user.UserName,
                ProfilePicture = user.ProfilePicture
            };
        }
    }
}