using System.Collections.Generic;
using System.Threading.Tasks;
using agroApp_BackEnd.API.models;

namespace agroapp_backend.API.Core.CategoryManager
{
    public interface ICategoryManager
    {
         Task<Category> CreateCategoryAsync(Category category);

        Task<IEnumerable<Category>> GetAllAsync();

        Category GetById(int idCategory);

        bool UpdateCategory(Category category);

        bool DeleteCategory(Category category);

    }
}