using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using agroApp_BackEnd.API.data.Repositories;
using agroApp_BackEnd.API.models;

namespace agroapp_backend.API.Core.CategoryManager
{
    public class CategoryManager : ICategoryManager
    {
        private readonly IRepositoryWrapper _repositoryWrapper;
        public CategoryManager(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }

        public async Task<IEnumerable<Category>> GetAllAsync()
        {
            return await _repositoryWrapper.Category.GetAllAsync(); 
        }

        public Category GetById(int idCategory)
        {
            return _repositoryWrapper.Category.FindByCondition(x => x.Id == idCategory).FirstOrDefault();
        }

        public async Task<Category> CreateCategoryAsync(Category category)
        {
            await _repositoryWrapper.Category.CreateAsync(category);
            await _repositoryWrapper.SaveChangesAsync();
            return category;
        }

        public bool UpdateCategory(Category category)
        {
            try
            {
                _repositoryWrapper.Category.Update(category);
                return true;
            }
           catch (Exception ex)
            {
                Console.WriteLine(ex);
            } 
            { return false; }
           
        }

        public bool DeleteCategory(Category category)
        {
            try
            {
                _repositoryWrapper.Category.Delete(category);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            } 
            { return false; }
        }

    }
}