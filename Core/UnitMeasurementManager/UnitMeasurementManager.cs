using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using agroApp_BackEnd.API.data.Repositories;
using agroApp_BackEnd.API.models;

namespace agroapp_backend.API.Core.UnitMeasurementManager
{
    public class UnitMeasurementManager : IUnitMeasurementManager
    {
        private readonly IRepositoryWrapper _repositoryWrapper;
        public UnitMeasurementManager(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }

        public async Task<IEnumerable<UnitMeasurement>> GetAllAsync()
        {
            return await _repositoryWrapper.unitMeasurement.GetAllAsync(); 
        }

        public UnitMeasurement GetById(int idUnitMeasurement)
        {
            return _repositoryWrapper.unitMeasurement.FindByCondition(x => x.Id == idUnitMeasurement).FirstOrDefault();
        }

        public async Task<UnitMeasurement> CreateUnitMeasurementAsync(UnitMeasurement unitMeasurement)
        {
            await _repositoryWrapper.unitMeasurement.CreateAsync(unitMeasurement);
            await _repositoryWrapper.SaveChangesAsync();
            return unitMeasurement;
        }

        public bool UpdateUnitMeasurement(UnitMeasurement unitMeasurement)
        {
            try
            {
                _repositoryWrapper.unitMeasurement.Update(unitMeasurement);
                return true;
            }
           catch (Exception ex)
            {
                Console.WriteLine(ex);
            } 
            { return false; }
           
        }

        public bool DeleteUnitMeasurement(UnitMeasurement unitMeasurement)
        {
            try
            {
                _repositoryWrapper.unitMeasurement.Delete(unitMeasurement);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            } 
            { return false; }
        }

    }
}