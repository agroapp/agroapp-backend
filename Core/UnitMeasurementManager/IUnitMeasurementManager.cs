using System.Collections.Generic;
using System.Threading.Tasks;
using agroApp_BackEnd.API.models;

namespace agroapp_backend.API.Core.UnitMeasurementManager
{
    public interface IUnitMeasurementManager
    {
        Task<UnitMeasurement> CreateUnitMeasurementAsync(UnitMeasurement unitMeasurement);

        Task<IEnumerable<UnitMeasurement>> GetAllAsync();

        UnitMeasurement GetById(int idUnitMeasurement);

        bool UpdateUnitMeasurement(UnitMeasurement unitMeasurement);

        bool DeleteUnitMeasurement(UnitMeasurement unitMeasurement);

    }
}