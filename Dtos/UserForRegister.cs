using System.ComponentModel.DataAnnotations;

namespace agroApp_BackEnd.API.Dtos
{
    public class UserForRegister
    {
        [Required(ErrorMessage = "El nombre de usuario es requerido.")]
        public string UserName { get; set; }

        [Required]
        [StringLength(16, MinimumLength = 6, ErrorMessage = "La longitud de contraseña es entre 4 a 16 caracteres.")]
        public string Password { get; set; }
    }
}