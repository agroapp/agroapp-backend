using agroApp_BackEnd.API.models;

namespace agroapp_backend.API.Dtos
{
    public class AddOrder
    {
        public int IdOrder { get; set; }

        public int UserVendorId { get; set; }

        public int UserBuyerId { get; set; }

        public string Description { get; set; }

        public int CategoryId { get; set; }

        public Product[] Products { get; set; }

    }
}