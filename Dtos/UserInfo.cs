using System.Collections.Generic;
using agroApp_BackEnd.API.models;

namespace agroapp_backend.API.Dtos
{
    public class UserInfo
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string ProfilePicture { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}