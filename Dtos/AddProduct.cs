using Microsoft.AspNetCore.Http;

namespace agroapp_backend.API.Dtos
{
    public class AddProduct
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public IFormFile File { get; set; }

        public string PhotoUrl { get; set; }

        public string PhotoId { get; set; }

        public int CategoryId { get; set; }

        public int UserId { get; set; }

        public float Price { get; set; }
    }
}